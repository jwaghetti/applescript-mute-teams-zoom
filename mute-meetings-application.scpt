on run {input, parameters}
	if application "Microsoft Teams" is running then
		activate application "Microsoft Teams"
		tell application "System Events" to tell process "Microsoft Teams" to keystroke "m" using {command down, shift down}
		tell application "System Events"
			tell process "Microsoft Teams"
				set frontmost to true
			end tell
		end tell
	end if
	
	if application "zoom.us" is running then
		activate application "zoom.us"
		tell application "System Events" to tell process "zoom.us" to keystroke "a" using {command down, shift down}
	end if
	
	return input
end run
