# mac-mute-meetings

AppleScripts to mute/unmute Microsoft Teams, Zoom and Google Meet (in Google Chrome).

If the desired application is running, the script activates it (brings to front) and sends the respective shortcut. Please notice it does not synchronize mute/unmute status in applications. Also, it is context dependent on what
the shortcut will do in the desired application. For example, if you open an Excel spreadsheet in Microsoft Teams,
the shortcut will not work because it will be delivered to Excel, not the Teams meeting.

## How to

- In Automator, open the file ``mute-teams-zoom-application.scpt`` and save it as an application named``mute-teams-zoom``.
- Also in Automator, open the file ``mute-teams-zoom-quick-action.scpt`` and save it as a quick action
- In ``System Preferences -> Keyboard -> Shortcuts -> Services``, you'll see the quick action created under *General*. Assign a shortcut
  to it and use it to mute/unmute Teams/Zoom.
- When running the application, you'll need to grant permissions for "Acessibility" and "Send System Events". This is necessary
  for mute-teams-zoom to send keystrokes to other applications. You can review this authorization in
  ``System Preferences -> Security & Privacy -> Privacy -> Automation`` and
``System Preferences -> Security & Privacy -> Privacy -> Accessibility``.


## License

Copyright © 2022 Jean Waghetti <jwaghetti@gmail.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
